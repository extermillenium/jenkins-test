#!groovy

@Library("devops-pipeline@master")

import com.roche.pfdevops.build.*
import com.roche.pfdevops.scm.*
import com.roche.pfdevops.common.PipelineContext
import com.roche.pfdevops.common.utils.*

devopsPipeline()
{
	catchError
	{
		branching.Model scmBranchModel = PipelineContext.instance.factory.newIntermediaryBranchModel(PipelineContext.instance.scmHandler)
		Builder mavenBuilder
	
		stageCheckout(stashLabel: "checkout-scm")
		{
			//PostBuild steps on checkout
		}

		nodeMaven(unstashLabel: "checkout-scm", version: "jdk9u1-3.5.2", parameters: ""){
			stage ("Maven Configure"){
				mavenBuilder = PipelineContext.instance.factory.newMavenBuilder(scmBranchModel, "test/maven/")
			}

			stage("Maven Build"){
				mavenBuilder.execute("clean compile")
			}

			stageThrowsOnBuildStatusFailure("Maven Test")
			{
				mavenBuilder.execute("test-compile")
				try {
					mavenBuilder.execute("test")
				} finally {
					mavenBuilder.archiveJunitResult("**/TEST-*xml")
				}

				try {
					mavenBuilder.execute("verify")
				} finally {
					mavenBuilder.publishSerenityResults()
				}
			}

			stageThrowsOnBuildStatusFailure("Maven Analyze"){
				PipelineContext.instance.factory.newJacocoCodeAnalyzer(mavenBuilder, [maximumLineCoverage:'30']).analyze()
			}
		
			mavenBuilder.publish([], false, true)
		
		}
	}
	executeIfBuildStatusNotSuccess
	{
		PipelineContext.instance.factory.newEmail().send()
	}
}