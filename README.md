# Jenkins Global Library
Jenkins Global Library Maven Release Test is a repository to include dummy projects to test builders and release usage of the Jenkins Global Library for maven projects.

## Repository properties
### Responsible
Ricard Forcada ricard.forcada@roche.com
### Continuous Integration location
http://jenkins.kiosk.roche.com/job/Projects/job/project-functions-devops/job/jenkins-global-pipeline-maven-release-test/
  
## Getting started
Check JenkinsFile and Jenkins Global Library documentation